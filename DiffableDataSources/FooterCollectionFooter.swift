
import UIKit

final class FooterCollectionHeader: UICollectionReusableView, LoadNidoble {
   
   
   private var viewModel: FooterViewModel!
   
   public func configure(viewModel: FooterViewModel){
      self.viewModel = viewModel
   }
   
}
