
import UIKit

final class HeaderCollectionHeader: UICollectionReusableView, LoadNidoble {
   
   
   private var viewModel: HeaderViewModel!
   
   public func configure(viewModel: HeaderViewModel){
      self.viewModel = viewModel
   }
   
}
